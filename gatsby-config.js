module.exports = {
  plugins: [
    {
      resolve: `gatsby-source-rss-feed`,
      options: {
        url: `http://feeds.feedburner.com/ucllc/brandnew?format=xml`,
        name: `BrandNew`,
        /*parserOption: {
          customFields: {
            content: ['content'],
            excerpt: ['contentSnippet'],
            title: [`title`],
            link: [`link`],
            date: [`isoDate(formatString: "ddd MMM YYYY)`],
            content: [`content`],
            id: [`id`]
          }
        }*/
      },
    },
    `gatsby-plugin-sass`,
  ],
}
