import React from "react"
import Feed from "../components/Feed"

const Home = ({data}) => {
  return (
    <div>
      <h1>RSS Home</h1>
      <Feed />
    </div>
  )
}

export default Home
