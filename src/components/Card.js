import React from "react"
import {Link} from "gatsby"

import Markdown from "markdown-to-jsx"

import styles from "./css/card.module.scss"

const Card = props => {
  const str = props.content
  const imgComponent = str.substring(
    str.indexOf(`<img src=`),
    str.indexOf(`"webfeedsFeaturedVisual`) + 27
  )
  const readingTime = parseInt(str.length / 1700, 10)
  const articleType = props.title.substring(0, props.title.indexOf(":"))
  const excerpt = str.substring(str.indexOf("<p>") + 3, str.indexOf("</p>"))
  const newTitle = props.title.substring(
    props.title.indexOf(":") + 1,
    props.title.lastIndexOf(props.title.slice(-1)) + 1
  )

  return (
    <div className={styles.card}>
      <Link to={props.slug}>
        <Markdown className={styles.imgWrapper}>{imgComponent}</Markdown>
        <div className={styles.infoWrapper}>
          <span className={styles.tag}>{articleType}</span>
          <span className={styles.date}>{props.date}</span>
          <a href={props.source} className={styles.source}>
            Source
          </a>
          {articleType === "Noted" ||
            (articleType === "Reviewed" && (
              <span className={styles.readingTime}>
                {readingTime < 1 ? "1" : readingTime} min read
              </span>
            ))}
          <h3 className={styles.title}>{newTitle}</h3>
          {/*articleType === "Noted" ||
            (articleType === "Reviewed" && (
              <Markdown className={styles.excerpt}>{excerpt}</Markdown>
			))*/}
        </div>
      </Link>
    </div>
  )
}

export default Card
