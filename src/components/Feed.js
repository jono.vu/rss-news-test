import React from "react"
import {graphql, StaticQuery} from "gatsby"

import slug from "slug"

import Card from "./Card"

import styles from "./css/feed.module.scss"

const Feed = () => (
  <StaticQuery
    query={graphql`
      query {
        allFeedBrandNew {
          edges {
            node {
              title
              guid
              isoDate(formatString: "h:mma ddd MMM YYYY")
              content
              id
            }
          }
        }
      }
    `}
    render={data => {
      return (
        <section className={styles.feed}>
          {data.allFeedBrandNew.edges.map(post => (
            <Card
              title={post.node.title}
              slug={`articles/${slug(post.node.title, {
                replacement: "-",
                symbols: true,
                remove: /[.]/g,
                lower: true,
                charmap: slug.charmap,
                multicharmap: slug.multicharmap,
              })}`}
              date={post.node.isoDate}
              content={post.node.content}
              //excerpt={post.node.contentSnippet}
              source={post.node.guid}
            />
          ))}
        </section>
      )
    }}
  />
)

export default Feed
